import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const config = {
  apiKey: "AIzaSyAskC3-qhoq0xLFKVtNSNAM_4quzGeDlFU",
  databaseURL: "https://astronauts.firebaseio.com/",
  projectId: "astronauts-f46c2",
  messagingSenderId: "393272129333"
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;