import reducer from '../../../store/reducers/assignments';
import * as types from '../../../store/actions/actionTypes';

describe('astronauts reducer', () => {
    const astronauts = [{
        guid: '21323-432bgfbhf-43543254334',
        name: {
            first: 'Yuri',
            last: 'Gagarin',
        },
        email: 'yuri.gagarin@ru.com',
        phone: '+353480954654'
    }];

    it('should return the initial state', () => {
        expect(reducer(undefined, [])).toEqual([]);
    });
});
