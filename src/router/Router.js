import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { AppInitilizerContainer } from '../components/AppInitializer/AppInitializer';

const AppRouter = () => (
  <Router>
    <Route path="/" component={AppInitilizerContainer} />
  </Router>
);

export default AppRouter;