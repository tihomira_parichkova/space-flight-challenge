import * as action from './actionTypes';
import firebase from '../../Firebase';



/**
 * Loading the astronauts
 * 
 * @param {Array} astronauts 
 */
export const loadAstronauts = (astronauts) => ({
  type: action.LOAD_ASTRONAUTS,
  astronauts,
});

/**
 * Loading the astronauts
 * 
 * @param {Array} astronauts 
 */
export const loadAssignments = (astronauts) => ({
  type: action.LOAD_ASSIGNMENTS,
  astronauts,
});


/**
 * Fetching the astronauts from Firebase,
 * then dispatching new action to save them in the store
 * 
 * @param {void} 
 */
export const fetchAstronauts = () => async (dispatch) => {
  const astronauts = [];

  const astronautsRef = await firebase.firestore().collection('astronauts').get().then(res => {
    res.docs.forEach(doc => {
      astronauts.push(doc.data())
    });
}).catch(function(error) {
    console.log("Error getting document:", error);
});

  dispatch(loadAstronauts(astronauts));
};
