import * as actions from '../actions/actionTypes';


/**
 * Handles austronauts data logic.
 *
 * @param  {Array} state: the new state
 * @param  {Object} action: action to be performed
 * @return {Object} the new state
  */
const astronauts = (state = [], action) => {
  switch (action.type) {
    case actions.LOAD_ASTRONAUTS: {
      return action.astronauts;
    }

    default:
      return state;
  }
};


export default astronauts;