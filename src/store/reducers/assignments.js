import * as actions from '../actions/actionTypes';


/**
 * Recursive function that mixes an array passed indexes,
 * returning whole new array with values on different than the previous indexes.
 * 
 * @param {Array} items 
 */
function mixAssignments(items) {
  let mixedAstrounauts = [];

  function recursive(items) {
    const deepClonedItems = JSON.parse(JSON.stringify(items)); // here lodash or underscore could be used, I prefered the old way
    const ind = Math.floor(Math.random()*deepCloned.length);
    
    while(deepClonedItems.length) {
      mixedAstrounauts.push(deepClonedItems.splice(ind, 1));

      return recursive(deepClonedItems);
      
    }
    
    return mixedAstrounauts;
  }


  recursive(items);

  return mixedAstrounauts.flat();
}

/**
 * Handles assignments logic.
 *
 * @param  {Array} state: the new state
 * @param  {Object} action: action to be performed
 * @return {Object} the new state
  */
const assignments = (state = [], action) => {
  switch (action.type) {
    case actions.LOAD_ASSIGNMENTS: {
      return mixAssignments(action.astronauts);
      ;
    }

    default:
      return state;
  }
};


export default assignments;