import { combineReducers } from 'redux';

import astronauts  from './astronauts';
import assignments  from './assignments';


const rootReducer = combineReducers({
  astronauts,
  assignments,
});


export default rootReducer;