import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import {
    fetchAstronauts
} from '../../store/actions';

import HomeScreenContainer from '../HomeScreen/HomeScreen';

import firebase from '../../Firebase';


/**
 * The main point of the application.
 * Receives data from its container.
 */
const AppInitilizer = ({ fetchAstronauts }) => {
  useEffect(() => {
      fetchAstronauts();
  }, [])

  return (
    <HomeScreenContainer />
  );
};

const AppInitilizerContainer = connect(
  null,
  {
    fetchAstronauts
  }
)(AppInitilizer);

export default AppInitilizer;

export { AppInitilizerContainer };