import React from 'react';
import PropTypes from 'prop-types';
import './SimpleButton.scss';

/**
 * SimpleButton
 *
 * It has text and onClick handler passed from its parent
 *
 * @param {String} text
 * @param {Function} onClick: Function handling clicking on the button
 */
const SimpleButton = ({text, onClick }) => {
  return (
    <button
      className="simple-button"
      onClick={onClick}
    >
      {text}
    </button>
)};

SimpleButton.propTypes = {
  text: PropTypes.string.isRequired,
};


export { SimpleButton };
