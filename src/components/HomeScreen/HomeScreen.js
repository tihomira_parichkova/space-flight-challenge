import React from 'react';
import { connect } from 'react-redux';
import { loadAssignments } from '../../store/actions';
import { SimpleButton } from '../SimpleButton/SimpleButton.js';
import './HomeScreen.scss';



/**
 * The main screen - called also HomeScreen
  *
 * @param {Array} astronauts - the list of all astronauts
 * @param {Array} assignments - the list of all astronauts to be watched over
 * @param {Function} onClick
 */
function HomeScreen({ astronauts, assignments, onClick }) {
  return (
    <div className="app">
      <header className="app-header">
        <SimpleButton
          text="Launch It Now"
          onClick={() => onClick(astronauts)}
        ></SimpleButton>
      </header>

      <div className="assignments-holder">
        {assignments.length === 0
          ? <p className="no-loaded">No assignments loaded...</p>
          : <>
            <div className="first-column">
              <div>Assignee:</div>
              <br></br>
              {astronauts.map(astronaut => {
                if (astronaut.name.first !== "Лайка") {

                  return (
                    <div key={astronaut.guid}>
                      {`${astronaut.name.first} ${astronaut.name.last}`}
                    </div>
                  );
                }
              })
              }
            </div>

            <div className="second-column">
              <div>Assignee:</div>
              <br></br>
              {assignments.map(item => {
                if (!item) {
                  return;
                };

                return (
                  <div key={item && item.guid}>
                    {`${item && item.name && item.name.first} ${item && item.name && item.name.last}`}
                  </div>
                )
              })}
            </div>
          </>
        }
      </div>
      <footer className="app-footer"></footer>
    </div>
  );
}

/**
 * Extracting needed data from the store
 * 
 * @param {Array} astronauts
 * @param {Array} assignments 
 */
const mapStateToProps = ({ astronauts, assignments }) => {
  return {
    astronauts,
    assignments,
  }
};

/**
 * Connecting the event handlers to actions
 * 
 * @param {Function} dispatch 
 */
const mapDispatchToProps = (dispatch) => {
  return {
    onClick: (astronauts) => {
      dispatch(loadAssignments(astronauts));
    },
  }
};


/**
 * Connectin the component to Redux store
 */
const HomeScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);


export default HomeScreenContainer;

export { HomeScreen };
