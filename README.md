## DEVELOPMENT NOTES ##

This apllication fetches the data from firebase
The Firebase RealTime Database returns the same results as given in the task.

The application runs with  `npm install` then `npm start` since it is bootstrapped with [Create React App] 

When you run the script in your console to start the development server
your default browser will open a window with very simple screen

Clicking on the button that says 'Launch It Now' will show the assigments

## Architecture ##

`src` folder has `components` where the components with their styles are
`src` folder has also `store` folder with `actions` and `reducers` folders where the actions and reducers       for the application logic live.
`src` also has `tests` folder

## TOKENS ##

Since this is a project that needs to be simple and easy to run
the APIKEY token and some other values needed for Firebase to identify the project
will be hardcoded. 
In production environment these variables could stay in CI/CD tool ( Octopus let's say or Travis CI)
and taken as variables from there.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.